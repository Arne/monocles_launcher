monocles launcher
======
An Android launcher not spending time and memory on stuff you'd rather do. 

<a href="https://apt.izzysoft.de/fdroid/index/apk/de.monocles.launcher"><img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" alt="Jetzt bei F-Droid" height="70"></a>

Forked from the [KISS Launcher](https://github.com/Neamar/KISS) but **without** Google or Bing search.

[Copylefted](https://en.wikipedia.org/wiki/Copyleft) libre software, licensed [GPLv3+](https://de.wikipedia.org/wiki/GNU_General_Public_License#GPL_Version_3):

Use, see, [change](CONTRIBUTING.md) and share at will; with all.

From _your_ background, type the first letters of apps, contact names, or settings—and click.  
Results clicked more often are promoted.

_Browsing for apps is and should be secondary_.

